#!/bin/bash

export MYUUID=$(uuid)
export NW2_LOGFILE=${MYUUID}.log

echo "NW2_LOGFILE for this job is " $NW2_LOGFILE

export LD_PRELOAD=${PWD}/nw2.so
echo "Preloading ${LD_PRELOAD} to intercept networking calls"
wget https://root.cern.ch/download/root_v6.12.06.Linux-ubuntu16-x86_64-gcc5.4.tar.gz -O - > /dev/null


