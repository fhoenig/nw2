#ifndef mymq_H
#define mymq_H

#include <sys/types.h>
#include <time.h>


enum nw2_call {
  nw2_read,
  nw2_pread,
  nw2_recv,
  nw2_recvfrom,
  nw2_recvmsg,
  nw2_write,
  nw2_pwrite,
  nw2_send,
  nw2_sendto,
  nw2_sendmsg,
  nw2_finish
};

struct nw2_message {
  enum nw2_call call;
  ssize_t nbytes;
  time_t time;
  pid_t pid;
  
};

#endif /* mymq_H */