#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <mqueue.h>
#include <unistd.h>
#include "nw2_msg.h"





int main() {
  char *mqname = getenv("NW2_MQNAME");
  if (!mqname) {
    printf("Need env variable NW2_MQNAME\n");
    exit(1);
  }
  if (mqname[0] != '/') {
    printf("env variable NW2_MQNAME must start with /\n");
    exit(1);
  }
  

  unsigned int prio;               // Priority 
  struct nw2_message msg;
  struct mq_attr attr;
  mqd_t mq;
  pid_t pid = getpid();
  time_t time_now = time(NULL);

  

  mq = mq_open(mqname, O_WRONLY, 0644, NULL);
  mq_getattr (mq, &attr);
  if (mq < 0) {
    perror("# Could not open message queue");
    exit(1);
  }
  
  printf("# opened mq %i with message size %li\n", mq, attr.mq_msgsize);


  msg.call = nw2_finish;
  msg.nbytes = 0;
  msg.pid = pid;
  msg.time = time_now;
    
  int ret = mq_send(mq, (char *) &msg, sizeof(msg), 0);
  if (ret < 0) {
    perror("");
    exit(1);
  } 
    mq_unlink(mqname);
  perror("# removing message queue");
  printf("shutdown command sent\n");
    

return 0;
}