#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <mqueue.h>
#include <unistd.h>
#include "nw2_msg.h"


int shutdown(pid_t pid, char *mqname) {
  printf("# received shutdown command from pid %i\n", pid);
  mq_unlink(mqname);
  exit(0);
  
}


int main() {

  char *mqname = getenv("NW2_MQNAME");
  if (!mqname) {
    printf("Need env variable NW2_MQNAME\n");
    exit(1);
  }
  if (mqname[0] != '/') {
    printf("env variable NW2_MQNAME must start with /\n");
    exit(1);
  }
  
  unsigned int prio;               // Priority 
  struct nw2_message msg;
  
  struct mq_attr attr;
  attr.mq_maxmsg = 10;
  attr.mq_msgsize = sizeof(msg);
  mqd_t mq = -1;
  mq = mq_open(mqname, O_CREAT | O_RDWR, 0644, &attr);
  //perror("# Opening queue with name %s", mqname);
  if (mq < 0) {
    printf("could not establich connection to message queue, exiting");
    exit(0);
  }
  
  mq_getattr (mq, &attr);
  
  printf("# opened mq %i with name %s and message size %li\n", mq, mqname, attr.mq_msgsize);
  ssize_t total = 0;
  unsigned int cnt = 0;
  while(1) {
     int ret  = mq_receive(mq, (char *) &msg, sizeof(msg), &prio);
     if (ret == -1) {
       perror("");
       continue;
     }
     total += msg.nbytes;
     
//      if (cnt++ % 1000 == 0){
//        printf("message reads: %i %li total %lu\n", msg.call, msg.nbytes, total);
//        
//      }
     
     char *funcname = "UNKNOWN";
     switch (msg.call) {
       case   nw2_read: funcname = "read"; break;
       case   nw2_pread:     funcname = "pread"; break;
       case   nw2_recv:      funcname = "recv"; break;
       case   nw2_recvfrom:  funcname = "recvfrom"; break;
       case   nw2_recvmsg:   funcname = "recvmsg"; break;
       case   nw2_write:     funcname = "write"; break;
       case   nw2_pwrite:    funcname = "pwrite"; break;
       case   nw2_send:      funcname = "send"; break;
       case   nw2_sendto:    funcname = "sendto"; break;
       case   nw2_sendmsg:   funcname = "sendmsg"; break;
       case   nw2_finish:    shutdown(msg.pid, mqname); break;
     }
       
     
     printf("%ld\t%d\t%s\t%ld\n", msg.time, msg.pid, funcname, msg.nbytes);
     
     //printf("receive mq? %i %i %i\n", ret, errno, EAGAIN);
     //printf("message reads: %i %li\n", msg.call, msg.nbytes);
     /* perror("");*/
  }
  printf("how did I end up here?");
  mq_unlink(mqname);
  return 0;
}
