#!/usr/bin/env python

from __future__ import print_function
import sys

nbytes_read = 0
nbytes_write = 0

funcnames_read = set(['read', 'pread', 'recv', 'recvfrom', 'recvmsg'])
funcnames_write = set(['write', 'pwrite', 'send', 'sendto', 'sendmsg'])

print("#timestamp\tnetread\tnetwrite")

with open(sys.argv[1]) as logfile:
    for line in logfile:
        if line.startswith("#") : continue
        
        timestamp, pid, funcname, nbytes = line.strip().split('\t')
        nbytes = int(nbytes)
        if nbytes < 0:
            continue
        #print(timestamp, pid, funcname, nbytes)
        if funcname in funcnames_read:
            nbytes_read += nbytes

        elif funcname in funcnames_write:
            nbytes_write += nbytes

        else:
            raise KeyError("Unknown function '{}'.format(funcname)")

        print("{}\t{}\t{}".format(timestamp, nbytes_read, nbytes_write))
